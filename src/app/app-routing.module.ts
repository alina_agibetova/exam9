import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoctailsComponent } from './coctails/coctails.component';
import { NewCoctailComponent } from './new-coctail/new-coctail.component';

const routes: Routes = [
  {path: '', component: CoctailsComponent},
  {path: 'new-coctail', component: NewCoctailComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
