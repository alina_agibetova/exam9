import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Cocktail } from './cocktail.model';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class CocktailService {
  cocktailUpLoading = new Subject<boolean>();
  cocktailChange = new Subject<Cocktail[]>();
  cocktailFetching = new Subject<boolean>();
  cocktailRemoving = new Subject<boolean>();
  private cocktails: Cocktail[] = [];


  constructor(private http: HttpClient){}


  fetchCocktails(){
    this.cocktailFetching.next(true);
    this.http.get<{[id: string]: Cocktail}>('https://alina-beaf9-default-rtdb.firebaseio.com/cocktail.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const cocktailData = result[id];
          return new Cocktail(
            id,
            cocktailData.name,
            cocktailData.image,
            cocktailData.nameOfCocktail,
            cocktailData.description,
            cocktailData.ingredients,
            cocktailData.instruction
          );
        })
      })).subscribe(cocktails => {
        this.cocktails = cocktails;
        this.cocktailChange.next(this.cocktails.slice());
        this.cocktailFetching.next(false);
    }, error => {
        this.cocktailFetching.next(false);
    })
  }

  getCocktailForm(cocktail: Cocktail){
    const body = {
      name: cocktail.name,
      image: cocktail.image,
      nameOfCocktail: cocktail.nameOfCocktail,
      description: cocktail.description,
      ingredients: cocktail.ingredients,
      instruction: cocktail.instruction,
    };

    this.cocktailUpLoading.next(true);
    return this.http.post('https://alina-beaf9-default-rtdb.firebaseio.com/cocktail.json', body).pipe(
      tap(() => {
        this.cocktailUpLoading.next(false);
      }, () => {
        this.cocktailUpLoading.next(false);
      })
    )
  }

  getCocktail(){
    return this.cocktails.slice();
  }

  fetchCocktail(id: string){
    return this.http.get<Cocktail | null>(`https://alina-beaf9-default-rtdb.firebaseio.com/cocktail/${id}.json`).pipe(
      map(result => {
        if (!result){
          return null;
        }

        return new Cocktail(
          id, result.name,
          result.image,
          result.nameOfCocktail,
          result.description,
          result.ingredients,
          result.instruction
        )
      })
    );
  }


  // removeIng(i: number, id: string){
  //   this.cocktailRemoving.next(true);
  //   return this.http.delete(`https://alina-beaf9-default-rtdb.firebaseio.com/cocktail/${id}/ingredients/${i}.json`).pipe(
  //     tap(() => {
  //       this.cocktailRemoving.next(false);
  //     }, () => {
  //       this.cocktailRemoving.next(false);
  //     })
  //   )
  //
  // }

}
