import { Component, OnDestroy, OnInit } from '@angular/core';
import { Cocktail } from '../cocktail.model';
import { Subscription } from 'rxjs';
import { CocktailService } from '../cocktail.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-coctails',
  templateUrl: './coctails.component.html',
  styleUrls: ['./coctails.component.css']
})
export class CoctailsComponent implements OnInit, OnDestroy{
  cocktails: Cocktail[] = [];
  cocktail!: Cocktail;
  isFetching = false;
  modalOpen = false;
  cocktailChangeSubscription!: Subscription;
  cocktailFetchingSubscription!: Subscription;


  constructor(private cocktailService: CocktailService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.cocktails = this.cocktailService.getCocktail();
    this.cocktailChangeSubscription = this.cocktailService.cocktailChange.subscribe((cocktails: Cocktail[])=> {
      this.cocktails = cocktails;
    });

    this.cocktailFetchingSubscription = this.cocktailService.cocktailFetching.subscribe((isFetching: boolean)=> {
      this.isFetching = isFetching;
    });

    this.cocktailService.fetchCocktails();
  }

  openCheckoutModal(id: string){
    this.modalOpen = true;
    this.cocktailService.fetchCocktail(id);
    this.cocktails.forEach(cocktail => {
      if (id === cocktail.id){
        this.cocktail = cocktail;
      }
    })
  }

  closeCheckoutModal(id: string){
    this.modalOpen = false;

  }

  ngOnDestroy(): void {
    this.cocktailChangeSubscription.unsubscribe();
    this.cocktailFetchingSubscription.unsubscribe();
  }

}
