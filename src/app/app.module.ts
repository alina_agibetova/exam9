import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoctailsComponent } from './coctails/coctails.component';
import { NewCoctailComponent } from './new-coctail/new-coctail.component';
import { ModalComponent } from './ui/modal/modal.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CocktailService } from './cocktail.service';
import { ValidateImageDirective } from './validate-image.directive';

@NgModule({
  declarations: [
    AppComponent,
    CoctailsComponent,
    NewCoctailComponent,
    ModalComponent,
    ValidateImageDirective

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [CocktailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
