export class Cocktail {
  constructor(
    public id: string,
    public name: string,
    public image: string,
    public nameOfCocktail: string,
    public description: string,
    public ingredients: Ingredients[],
    public instruction: string,
  ){}
}

export class Ingredients {
  constructor(
    public ingName: string,
    public amount: number,
    public size: string,
  ){}
}
