import { Component, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  @Input() title = 'This is a modal';
  @Input() isOpen = false;
  @Output()close = new Subject<void>();

  onClose(){
    this.close.next();
  }


  constructor() { }

  ngOnInit(): void {
  }

}
