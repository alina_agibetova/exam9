
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { Directive } from '@angular/core';

export const imageValidator = (control: AbstractControl): ValidationErrors | null => {
  const hasImage = /((http|https)\:\/\/)/.test(control.value);


  if (hasImage) {
    return null;
  }

  return {image: true};
};

@Directive({
  selector: '[appImage]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidateImageDirective,
    multi: true
  }]
})
export class ValidateImageDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return imageValidator(control);
  }
}
