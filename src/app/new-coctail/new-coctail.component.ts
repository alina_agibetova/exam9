import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Cocktail } from '../cocktail.model';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CocktailService } from '../cocktail.service';
import { imageValidator } from '../validate-image.directive';

@Component({
  selector: 'app-new-coctail',
  templateUrl: './new-coctail.component.html',
  styleUrls: ['./new-coctail.component.css']
})
export class NewCoctailComponent implements OnInit, OnDestroy {
  isUpLoading = false;
  cocktailUpLoadingSubscription!: Subscription;
  cocktails!: Cocktail;
  cocktailForm!: FormGroup;
  editedId = '';
  isClick = true;

  constructor(private router: Router, private cocktailService: CocktailService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.cocktailForm = new FormGroup({
      name: new FormControl('', Validators.required),
      image: new FormControl('', [Validators.required, imageValidator]),
      nameOfCocktail: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      ingredients: new FormArray([]),
      instruction: new FormControl('', Validators.required)
    });

    this.cocktailUpLoadingSubscription = this.cocktailService.cocktailUpLoading.subscribe((isUpLoading: boolean)=> {
      this.isUpLoading = isUpLoading;
    })
  }

  fieldHasError(fieldName: string, errorType: string){
    const field = this.cocktailForm.get(fieldName);
    return field && field.touched && field.errors?.[errorType];
  }

  addIngredients(){
    this.isClick = false;
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    const ingGroup = new FormGroup({
      ingName: new FormControl('', Validators.required),
      amount: new FormControl(null, Validators.required),
      size: new FormControl('', Validators.required)
    });
    ingredients.push(ingGroup);
  }

  getIngredientsControls(){
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    return ingredients.controls;
  }

  onSubmit(){
    const id = this.editedId || Math.random().toString();
    const form = new Cocktail(id, this.cocktailForm.value.name,
      this.cocktailForm.value.image, this.cocktailForm.value.nameOfCocktail,
      this.cocktailForm.value.description, this.cocktailForm.value.ingredients, this.cocktailForm.value.instruction);
    const next = () => {
      this.cocktailService.fetchCocktails();
      void this.router.navigate(['/']);
    };

      this.cocktailService.getCocktailForm(form).subscribe(next);

  }

  OnDelete(i:number) {
    const remove = <FormArray>this.cocktailForm.get('ingredients');
    remove.removeAt(i);
  }

  ngOnDestroy(): void {
    this.cocktailUpLoadingSubscription.unsubscribe();
  }

}
